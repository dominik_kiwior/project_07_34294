 

Prowadzący: mgr inż. Tomasz Gądek 

Autorzy : Kiwior Dominik ( 34294 ) , Damian Majka ( 34305 ), Jaszczur Katarzyna(34289) 

Kierunek : Informatyka 


 

 

Opis funkcjonalności 

Początkowo otrzymujemy ekran z pierwszym pytaniem oraz możliwością wyboru kół ratunkowych po wpisaniu “5”.

Koło ratunkowe pół na pół umożliwia wyeliminowanie 2 niepoprawnych odpowiedzi.

Koło ratunkowe Pytanie do publiczności umożliwia nam oddanie pytania dla publiki. Dla poprawnej odpowiedzi losowana jest wartość z przedziału 50% - 100% a pozostała pula dzielona jest na resztę pytań.

Koło Telefon do przyjaciela które podaje nam poprawną odpowiedź jeżeli jesteśmy w max 5 pytaniu później losowana jest odpowiedź która zostanie wyświetlona.

Powyżej 5 pytania odpowiedź z telefonu przyjaciela jest losowa.


Narzędzia 

Do stworzenia aplikacji został użyty program IntelliJ oraz standardowe klasy służące do odczytu z pliku (File Reader), oraz losowanie randomowych wartości (Random). 


Podział pracy :

Dominik Kiwior 

-Dodanie projektu na bitbucket 

-menu wyboru odpowiedzi i kół ratunkowych 

-metoda sprawdzająca poprawność odpowiedzi  

-metoda wyświetlająca nagrodę oraz monit po przegranej 

-metoda pytanie do publiczności 

Katarzyna Jaszczur 

-pliki csv z pytaniami, 

-metoda koła ratunkowego telefon do przyjaciela  

-metoda koła ratunkowego pół na pół 

Damian Majka 

-Metoda otwierającą i pobierającą pytania z pliku .csv i losująca je 

-metoda wypisująca pytania na ekran 

-ogólny zamysł projektu( zmienne,  struktura , konstruktory) 

 