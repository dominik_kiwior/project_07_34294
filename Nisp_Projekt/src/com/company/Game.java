package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Game {
    private int lvl;
    private boolean half;
    private boolean questionToAudience;
    private boolean callSaul;
    private String question;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private String correctAnswer;
    private int[] rewards = {50,100,200,500,1000,2000,5000,10000,20000,40000,75000,125000,250000,500000,1000000};

    public Game() {
        this.lvl = 1;
        this.half = true;
        this.questionToAudience = true;
        this.callSaul = true;
        openFile(lvl);
        answer();
    }

    public void openFile(int lvl) {
        Random random = new Random();
        int questionNumber = random.nextInt(10);
        String file = lvl + ".csv";
        try (BufferedReader fileReader
                     = new BufferedReader(new FileReader(file))) {
            String line = "";
            for (int lineNumber = 1; lineNumber <= 12; lineNumber++) {
                if (lineNumber == questionNumber + 2) {
                    line = fileReader.readLine();
                } else
                    fileReader.readLine();
            }
            String[] tokens = line.split(",");
            this.question = tokens[1];
            this.answer1 = tokens[2];
            this.answer2 = tokens[3];
            this.answer3 = tokens[4];
            this.answer4 = tokens[5];
            this.correctAnswer = tokens[6];
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        showQuestions();
    }
    public void usageHalf() {
        if (this.half == true) {
            this.half = false;
            Random random = new Random();

            int randomAnswer = random.nextInt(4);
            randomAnswer++;
            while(randomAnswer == Integer.parseInt(this.correctAnswer)){
                randomAnswer = random.nextInt(4);
                randomAnswer++;
            }
            if(randomAnswer == 1){
                System.out.println(this.answer1);
            }
            else if(randomAnswer == 2){
                System.out.println(this.answer2);
            }
            else if(randomAnswer == 3) {
                System.out.println(this.answer3);
            }
            else if(randomAnswer == 4){
                System.out.println(this.answer4);
            }
            if(Integer.parseInt(this.correctAnswer) == 1){
                System.out.println(this.answer1);
            }
            else if(Integer.parseInt(this.correctAnswer)==2){
                System.out.println(this.answer2);
            }
            else if (Integer.parseInt(this.correctAnswer)==3){
                System.out.println(this.answer3);
            }
            else if (Integer.parseInt(this.correctAnswer)==4){
                System.out.println(this.answer4);
            }
        } else{
            System.out.println("Już użyłeś tego koła ratunkowego");
        }

    }
    public void usageBetterCallSaul(){
        if (this.callSaul == true) {
            this.callSaul = false;
            if(this.lvl < 6){
                if(Integer.parseInt(this.correctAnswer) == 1){
                    System.out.println("Poprawna odpowiedź to odpowiedź 1");
                }
                else if(Integer.parseInt(this.correctAnswer) == 2){
                    System.out.println("Poprawna odpowiedź to odpowiedź 2");
                }
                else if(Integer.parseInt(this.correctAnswer) == 3){
                    System.out.println("Poprawna odpowiedź to odpowiedź 3");
                }
                else if(Integer.parseInt(this.correctAnswer) == 4){
                    System.out.println("Poprawna odpowiedź to odpowiedź 4");
                }
            }
            else{
                Random random = new Random();
                int randomAnswer = random.nextInt(4);
                randomAnswer++;
                System.out.println("Nie jestem pewny ale poprawną odpowiedzią może być odpowiedź nr: " + randomAnswer);
            }
        }
        else{
            System.out.println("Już użyłeś tego koła ratunkowego");
        }
    }

    private void showQuestions() {
        System.out.println("Twoje pytanie to: " + this.question + "\n" +
                "\t odpowiedź numer 1 =" + this.answer1 + "\n" +
                "\t odpowiedź numer 2 =" + this.answer2 + "\n" +
                "\t odpowiedź numer 3 =" + this.answer3 + "\n" +
                "\t odpowiedź numer 4 =" + this.answer4 + "\n" +
                " Podaj odpowiedź lub wybierz koło ratunkowe\n Aby wyświetlić liste kół wpisz 5.");
    }

    private void checkAnswer(int userAnswer) {
        if (Integer.parseInt(this.correctAnswer) == userAnswer) {
            System.out.println("Zgadłeś, następne pytanie to :");
            showRewards(this.lvl);
            this.lvl += 1;
            openFile(this.lvl);
        } else {
            lose();
        }
    }

    public void usageQuestionToAudience(){
        if (this.questionToAudience == true) {
            this.questionToAudience = false;
            Random random = new Random();
            int resultOfCorrectAnswer = random.nextInt(50);
            resultOfCorrectAnswer+= 50;
            int resultOfFirstWrongAnswer = random.nextInt(100-resultOfCorrectAnswer);
            int resultOfSecondWrongAnswer = random.nextInt(100-resultOfCorrectAnswer-resultOfFirstWrongAnswer);
            int resultOfThirdWrongAnswer = 100-resultOfCorrectAnswer-resultOfFirstWrongAnswer-resultOfSecondWrongAnswer;
            if(Integer.parseInt(this.correctAnswer) == 1){
                System.out.println("odpowiedź 1 = " + resultOfCorrectAnswer);
                System.out.println("Odpowiedź 2 = " + resultOfFirstWrongAnswer);
                System.out.println("Odpowiedź 3 = " + resultOfSecondWrongAnswer);
                System.out.println("Odpowiedź 4 = " + resultOfThirdWrongAnswer);
            }
            else if(Integer.parseInt(this.correctAnswer) == 2){
                System.out.println("Odpowiedź 1 = " + resultOfFirstWrongAnswer);
                System.out.println("odpowiedź 2 = " + resultOfCorrectAnswer);
                System.out.println("Odpowiedź 3 = " + resultOfSecondWrongAnswer);
                System.out.println("Odpowiedź 4 = " + resultOfThirdWrongAnswer);
            }
            else if(Integer.parseInt(this.correctAnswer) == 3){
                System.out.println("Odpowiedź 1 = " + resultOfFirstWrongAnswer);
                System.out.println("Odpowiedź 2 = " + resultOfSecondWrongAnswer);
                System.out.println("odpowiedź 3 = " + resultOfCorrectAnswer);
                System.out.println("Odpowiedź 4 = " + resultOfThirdWrongAnswer);
            }
            else if(Integer.parseInt(this.correctAnswer) == 4){
                System.out.println("Odpowiedź 1 = " + resultOfFirstWrongAnswer);
                System.out.println("Odpowiedź 2 = " + resultOfSecondWrongAnswer);
                System.out.println("Odpowiedź 3 = " + resultOfThirdWrongAnswer);
                System.out.println("odpowiedź 4 = " + resultOfCorrectAnswer);
            }



        }
        else{
            System.out.println("Już użyłeś tego koła ratunkowego");
        }
    }

    private void answer() {
        int option = 0;
        int option1 = 0;
        do {
            Scanner scanner = new Scanner(System.in);
            option = scanner.nextInt();
            switch (option) {
                case 1:
                    checkAnswer(1);
                    break;
                case 2:
                    checkAnswer(2);
                    break;
                case 3:
                    checkAnswer(3);
                    break;
                case 4:
                    checkAnswer(4);
                    break;
                case 5:
                    System.out.println("Wybierz kolo ratunkowe : ");
                    if(this.half == true){
                        System.out.println("1 - Pół na Pół");
                    }
                    if(this.questionToAudience == true){
                        System.out.println("2 - Pytanie do publiczności");
                    }
                    if(this.callSaul){
                        System.out.println("3 = Telefon do przyjaciela");
                    }
                    Scanner scanner1 = new Scanner(System.in);
                    option1 = scanner1.nextInt();
                    switch (option1){
                        case 1:
                            usageHalf();
                            showQuestions();
                            break;
                        case 2:
                            usageQuestionToAudience();
                            showQuestions();
                            break;
                        case 3:
                            usageBetterCallSaul();
                            showQuestions();
                            break;
                    }
                    break;
                default:
                    System.out.println("Zła opcja.");
            }

        } while (option != 6);
    }

    private void showRewards(int lvl){
        System.out.println("Wygrałeś : " + this.rewards[lvl-1] + "zł");
        if(lvl==15){
            System.out.println("Wygrałeś MILION gratulacje!!!");
            System.exit(1);
        }
    }
    private void lose(){
        System.out.println("Przegrałeś!!! Straciłeś wszystkie pieniądze !");
    }

}
